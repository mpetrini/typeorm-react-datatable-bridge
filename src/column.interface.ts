import { SelectQueryBuilder } from 'typeorm';

export interface DatatableColumn<S> {
  alias: string;
  defaultOrder?: boolean;
  defaultOrderDirection?: 'ASC'|'DESC';
  where?: (query: SelectQueryBuilder<S>, parameter: any) => SelectQueryBuilder<S>|Promise<SelectQueryBuilder<S>>|undefined;
  setParameter?: (value: any) => any;
}
