export interface DatatableResponse<T> {
    data: any[];
    length: number;
}
