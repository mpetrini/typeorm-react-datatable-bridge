import { classToPlain } from 'class-transformer';
import { Request } from 'express';
import { SelectQueryBuilder } from 'typeorm';
import { DatatableColumn } from './column.interface';
import { DatatableResponse } from './datatable.response';
import { DatatableResultsOptions } from './options.interface';

export abstract class AbstractDatatable<T> {
  protected options: DatatableResultsOptions = {};

  constructor(protected readonly request: Request) {
  }

  protected abstract getColumns(): DatatableColumn<T>[];

  protected abstract getBaseQuery(): Promise<SelectQueryBuilder<T>>;

  private filterValues(
    query: SelectQueryBuilder<T>,
    column: DatatableColumn<T>,
    value: any
  ): Promise<SelectQueryBuilder<T>> {
    if (!column.where) {
      return Promise.resolve(query);
    }

    const parameter = column.setParameter
      ? column.setParameter(value)
      : value;

    return Promise.resolve(column.where(query, parameter))
      .then((cloneQuery) => {
          return cloneQuery!.setParameter(
            column.alias,
            parameter,
          );
      })
  }

  private where(query: SelectQueryBuilder<T>): Promise<SelectQueryBuilder<T>> {
    const columns = this.getColumns();
    const filters = this.request.query.filters as unknown as {[key: string]: any}|undefined;

    if (filters) {
      return Promise.all(columns.map(async (column) => {
        if (filters[column.alias]) {
          query = await this.filterValues(query, column, filters[column.alias]);
        }
      })).then(() => query);
    }

    return Promise.resolve(query);
  }

  private lengthAndOffset(query: SelectQueryBuilder<T>): SelectQueryBuilder<T> {
    const paginationPerPage: number = this.request.query.paginationPerPage as unknown as number;
    const page: number = this.request.query.page as unknown as number;

    return query.offset(paginationPerPage * (page - 1))
      .limit(paginationPerPage)
  }

  private sort(query: SelectQueryBuilder<T>): SelectQueryBuilder<T> {
    const sort: string = this.request.query.sort as unknown as string;
    const sortDirection: string = this.request.query.sortDirection as unknown as string;

    if (sort) {
      query.addOrderBy(
        `${query.alias}.${sort}`,
        sortDirection.toUpperCase() as 'ASC'|'DESC',
      );
    } else {
      this.getColumns().map((column) => {
        if (column.defaultOrder) {
          query.orderBy(
            `${query.alias}.${column.alias}`,
            (column.defaultOrderDirection || 'ASC').toUpperCase() as 'ASC'|'DESC',
          );
        }
      });
    }

    return query;
  }

  protected transform(data: T): Promise<T> {
    return Promise.resolve(data);
  }

  results(options?: DatatableResultsOptions): Promise<DatatableResponse<T>> {
    if (options) {
      this.options = options;
    }

    return Promise.resolve(this.getBaseQuery()).then(query => {
      return this.sort(
        this.lengthAndOffset(query)
      );
    }).then(query => this.where(query))
      .then(query => query.getManyAndCount())
      .then(results => {
        return Promise.all(results[0].map(async (temp: any) => {
          return classToPlain(await this.transform(temp));
        })).then((data) => ({
          data,
          length: results[1],
        }))
      });
  }
}
